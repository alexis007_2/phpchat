<?php
session_start();
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=watagame;charset=utf8', 'root', 'admin');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Chat</title>
</head>
<body>
<h1> Bienvenue sur le Chat </h1>	

<div id="conversation">
	<?php
	$id_utilisateur = $_SESSION['id_utilisateur'];
	$req = $bdd->prepare('SELECT * FROM message JOIN utilisateur ON message.id_utilisateur = utilisateur.id_utilisateur');
	$req-> execute();
	$reponse = $req->fetchAll(PDO::FETCH_ASSOC);
	if($reponse)
		{?>
	<?php foreach ($reponse as $r):?>
	<p><strong><?=$r['pseudo']?></strong> : <?=$r['contenu']?></p>	
	<?php endforeach?>
	<?php
		} 
		else
		{
			echo 'pas de message';
		}
		?>
</div>
<div id="message">
<form action="traitement2.php" method="post">
<p> Mon message </p>
<textarea name="contenu">
</textarea>
<input type="submit" value="envoyer"/>
</form>
</div>

<div id="profil" style="border:2px solid green;">
	<?php
	$id_utilisateur = $_SESSION['id_utilisateur'];
	$req = $bdd->prepare('SELECT * FROM utilisateur WHERE id_utilisateur=:id_utilisateur');
	$req-> execute(array(':id_utilisateur'=>$id_utilisateur));
	$reponse = $req->fetch(PDO::FETCH_ASSOC);?>
	<ul>
		<li> nom utilisateur : <?=$reponse['pseudo']?></li>
		<li>En ligne :<?=$reponse['online']?></li>
	</ul>	
</div>
<button id="monbouton">petit bouton</button>
<div id="moncontenu">
</div>
 <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script>
  	$(function() {
  		$('#monbouton').click(function() {
  			$.ajax({
  				type: 'GET',
  				dataType : "json",
  				url: 'insertmessage.php',
  				timeout: 3000
  			})

  			.done(function(data){
  				$("<p><strong>"+data.name+" </strong> va à "+data.ville+" </p>").appendTo("#moncontenu");
/*  				$.each(data, function(i, d) {
  					alert(d.name);

  					$('<tr><td>'+d.name+'</td><td>'+d.ville+'</td><td>'+d.rer+'</td></tr>').appendTo($('tbody'));

  				});
  				$('#loader').remove();*/

  			})

  			.fail(function(data){
  				alert('Matraque bidon dans le MI84');
  			})
  		});
  	});

</script> 
</body>
</html>