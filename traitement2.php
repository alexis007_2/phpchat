<?php
session_start();
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=watagame;charset=utf8', 'root', 'admin');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}


if((!empty($_POST['contenu'])))
{
	$contenu = $_POST['contenu'];
	$id_utilisateur = $_SESSION['id_utilisateur'];
	$req = $bdd->prepare('INSERT INTO message (contenu,date_message,id_utilisateur) VALUES (:contenu,NOW(),:id_utilisateur)');
	$req->debugDumpParams();
	$req-> execute(array(':contenu'=>$contenu,':id_utilisateur'=>$id_utilisateur));
	header('Location: chat.php');

}
else
{
	header('Location: chat.php');
}	
	