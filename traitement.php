<?php
session_start();
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=watagame;charset=utf8', 'root', 'admin');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}


if((!empty($_POST['pseudo']))&&(!empty($_POST['password'])))
{
	$pseudo = $_POST['pseudo'];
	$password = $_POST['password'];
	$req = $bdd->prepare('SELECT * FROM utilisateur WHERE pseudo=:pseudo AND password=:password');
	$req-> execute(array('pseudo'=>$pseudo,'password'=>$password));
	$reponse = $req->fetch(PDO::FETCH_ASSOC);
	if($reponse)
	{
		$_SESSION['id_utilisateur']=$reponse['id_utilisateur'];
		$_SESSION['pseudo']=$reponse['pseudo'];
		$id_utilisateur = $reponse['id_utilisateur'];
		$req = $bdd->prepare('UPDATE utilisateur SET online = 1 WHERE id_utilisateur =:id_utilisateur');
		$req-> execute(array(':id_utilisateur'=>$id_utilisateur));
		header('Location: chat.php');
	}
	else
	{
		header('Location: login.php');
	}
}
else
{
	header('Location: login.php');
}